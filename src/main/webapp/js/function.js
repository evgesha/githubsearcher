const panel = document.createElement('div');
const input = document.createElement('input');
const list = document.querySelector('.baseForPanel');
const form = document.createElement('form');
const button = document.createElement('button');


function refreshSelect(select, list) {
    while (select.firstChild) {
        select.removeChild(select.firstChild);
    }
    addOption(list, select);
}

function createSelect(list, options) {    
    const select = document.createElement('select');    
    select.size = list.length < options.maxSize ? list.length : options.maxSize;
    addOption(list, select);
    return select;
}

function addOption(list, select) {
    list.forEach((element) => {
        select.add(new Option(element.label, element.id));
    });
}

function createComboBox(list, towns) {    
    let str = '';  
    const countElementPanel = 5;
    const select = createSelect(towns, {maxSize: countElementPanel});
    panel.classList.add('panel');
    input.type = 'text';
    input.name = "query";
    input.minLength = "1";
    input.required = "required";
    input.autocomplete="off";
    form.action = "/codehelper/search";
    form.method = "POST";
	form.name = "form-search";

    input.classList.add('input');
    button.classList.add('button');
    select.classList.add('list');
    select.classList.add('clear');
    form.appendChild(input);
    form.appendChild(button);
    form.appendChild(select);
    list.appendChild(panel);
    panel.appendChild(form);

    panel.addEventListener('click', touchPanel);
    input.addEventListener('keyup', inputProcessing);
    select.addEventListener('click', selectPros);
    form.addEventListener('submit', function () {
                document.getElementsByClassName('page-preloader')[0].classList.add('visible');
            });


    function touchPanel(event) {
        const target = event.target;

        event.stopPropagation();
        if (target !== input && target !== button || !select.classList.contains('clear')) {
            return;
        }

        if (target !== input && select.classList.contains('clear')) {
            //document.location.href = 'result.html';
        }
        select.classList.remove('clear');
        str = input.value;
        input.value = '';
        input.focus();
        window.addEventListener('resize', closeList);
        window.addEventListener('scroll', closeList);
        document.body.addEventListener('click', closeList);
    }

    function inputProcessing() {
        const value = input.value;
        const search = towns.filter((element) => {
            return element.label.toLowerCase().indexOf(value.toLowerCase()) === 0;
        });
        refreshSelect(select, search);
    }


function closeList(event) {
        if(select.classList.contains('clear')) {
            return;
        }
        input.value = str;
        select.classList.add('clear');
        refreshSelect(select, towns);
        window.removeEventListener('resize', closeList);
        window.removeEventListener('scroll', closeList);
        document.body.removeEventListener('click', closeList);
    }

function selectPros(event) {
        const target = event.target;
        if(target.tagName.toLowerCase() !== "option") {
            return;
        }
        str = target.text;
        input.value = str;
        select.classList.add('clear');
        refreshSelect(select, towns);
        window.removeEventListener('resize', closeList);
        window.removeEventListener('scroll', closeList);
        document.body.removeEventListener('click', closeList);

    }
}

createComboBox(list, town);


