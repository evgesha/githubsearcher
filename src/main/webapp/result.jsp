<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<head>
<meta charset="UTF-8">    
<title>Result</title>
<link rel="stylesheet" type="text/css"  href="css/styleSecondPage.css">
<script src="js/function.js" type="application/javascript"></script>
</head>  
<body>
<div class="picture"> 
<p><a href="page.html"><img src="img/load.gif" width="200" alt=""></a></p>
</div>
<h1>Search results</h1>
<table>
<c:forEach items="${response}" var="entry">
<tr><td><a href=${entry.key}>"${entry.key}"</a></td></tr>
</c:forEach>
</table>
<body>
</html>