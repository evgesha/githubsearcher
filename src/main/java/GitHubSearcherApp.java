import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import utilits.JSONUtil;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import static utilits.JSONUtil.getJsonArrayFromURL;
import static utilits.JSONUtil.getJsonObjFromString;


public class GitHubSearcherApp {
    private static JSONObject itog = new JSONObject();
    private static String response;
    static int count = 0;


    public static void main(String[] args) throws ParseException, InterruptedException, IOException {

        URL url = new URL("https://api.github.com/search/repositories?q=language:java&client_id=1f352b366308bef9bfe0&client_secret=9bb2346ab416576ce2e8851f99553b5345c1505d");
        URLConnection hc = url.openConnection();
        try (Scanner scanner = new Scanner(hc.getInputStream());) {
            response = scanner.useDelimiter("\\A").next();
        }

        JSONObject page = getGitHubRepositoriesPage(1);
        JSONArray items = getJSONArrayProjectsFromPage(page);
        for (int i = 0; i < 1; i++) {
            JSONObject project = (JSONObject) items.get(i);
            JSONArray content = getJSONArrayContent(project);
            collectDataFromArray(content);
            JSONUtil.writeJsonToFile(itog, "itog.json");
        }
        System.out.println(count);
    }


    public static JSONObject getGitHubRepositoriesPage(Integer page) throws ParseException, InterruptedException {
        return getJsonObjFromString(response);
    }

    public static JSONArray getJSONArrayProjectsFromPage(JSONObject jsonPage) throws InterruptedException {
        return (JSONArray) jsonPage.get("items");
    }

    public static JSONArray getJSONArrayContent(JSONObject jsonObject) throws ParseException, InterruptedException {
        String contentURL = (String) jsonObject.get("contents_url");
        contentURL = contentURL.substring(0, contentURL.length() - "{+path}".length());
        return getJsonArrayFromURL(contentURL);
    }


    public static void collectDataFromArray(JSONArray jsonArray) throws ParseException, InterruptedException {
        for (int i = 0; i < jsonArray.size(); i++) {
            JSONObject jsonObject = (JSONObject) jsonArray.get(i);
            collectDatafromJSON(jsonObject);
        }
    }


    private static void collectDatafromJSON(JSONObject jsonObject) throws ParseException, InterruptedException {
        if (jsonObject.containsKey("type")) {
            String type = (String) jsonObject.get("type");
            if (type.contentEquals("file")) {
                String filename = (String) jsonObject.get("name");
                if (filename.endsWith(".java")) {
                    JSONObject tempJson = new JSONObject();
                    tempJson.put("name", filename);
                    String download_url = (String) jsonObject.get("download_url");
                    download_url = urlStringFormater(download_url);
                    System.out.println(download_url);
                    tempJson.put("download_url", download_url);
                    itog.put(jsonObject.get("sha"), tempJson);
                    count++;
                }
            } else {
                if (type.contentEquals("dir")) {
                    String url = (String) jsonObject.get("url");
                    url = urlStringFormater(url);
                    JSONArray jsonDirArray = getJsonArrayFromURL(url);
                    collectDataFromArray(jsonDirArray);
                }
            }
        }
    }

    private static String urlStringFormater(String wrongURL) {
        return wrongURL.replace("\\", "");
    }
}



