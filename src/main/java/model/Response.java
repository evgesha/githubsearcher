package model;

import org.elasticsearch.action.search.SearchResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import searchengine.SearchClient;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Set;

public class Response {

    public static HashMap<String, String> getResponseHashMap(String keyword) throws UnknownHostException {
        SearchResponse searchResponse = SearchClient.getSearchResponce(keyword);
        JSONObject responseJSON = new JSONObject(searchResponse.toString());
        JSONObject hitsJSON = responseJSON.getJSONObject("hits");
        JSONArray hitsArray = hitsJSON.getJSONArray("hits");
        HashMap<String, String> responseHashMap = new HashMap<>();
        for (int i = 0; i < hitsArray.length(); i++) {
            JSONObject hit = hitsArray.getJSONObject(i);
            JSONObject source = hit.getJSONObject("_source");
            if (!source.has("collect_set(link)")) {
                String link = source.get("link").toString();
                link = getLinkToGitHub(link);
                String code = source.get("text").toString();
                code = code.replaceAll("\n", "</br>");
                code = code.replaceAll(keyword, "<span class=\"keyword\">" + keyword + "</span>");
                responseHashMap.put(link, code);
            }
        }
        return responseHashMap;
    }

    public static String getLinkToGitHub(String webapilink) {
        String wrongLink = webapilink;
        wrongLink = wrongLink.replace("raw.githubusercontent", "github");
        int blobindex = wrongLink.indexOf("/",
                wrongLink.indexOf("/", wrongLink.indexOf("/", "https:///".length()) + 1) + 1);
        String gitHubLink = wrongLink.substring(0, blobindex) + "/blob" + wrongLink.substring(blobindex);
        return gitHubLink;
    }
}
