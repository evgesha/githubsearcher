package ResultFileWriter;

import model.Response;
import org.json.simple.parser.ParseException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GeneratePageResult {
    private String patchPage = "src/webapp/result.jsp";

    public void writeInFile(String nameRequest, HashMap<String, String> ResultList) throws IOException {
        FileWriter writer = new FileWriter(patchPage, false);
        writer.write("<!DOCTYPE html>\n" +
                "<head>\n" +
                "<meta charset=\"UTF-8\">    \n" +
                "<title>Result</title>\n" +
                "<link rel=\"stylesheet\" type=\"text/css\" href=\"css/styleSecondPage.css\">\n" +
                "</head>  \n" +
                "<body>\n" +
                "<h1>Результаты по запросу \"" + nameRequest + "\"</h1>\n"
                + "<table>\n" +
                "<tr>\n" +
                "<th>№</th>\n" +
                "<th>Ссылка на проект</th>\n" +
                "</tr>");
        int count = 1;
        for(Map.Entry<String, String> item : ResultList.entrySet()){
            writer.write("<tr><td>" + count + "</td><td><a href=\"" +
                    "</a></td><td><a href=\"" + item.getKey() +
                    "\">" + item.getKey() + "</a></td></tr>\n");
            count++;
        }
        writer.write("</table>\n" +
                "<body>\n" +
                "</html>");
        writer.flush();
    }

    public String getPatchPage() {
        return patchPage;
    }

    public void setPatchPage(String patchPage) {
        this.patchPage = patchPage;
    }
}
