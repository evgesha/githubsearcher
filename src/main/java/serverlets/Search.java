package serverlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Response;

@WebServlet(name = "serverlets.Search")
public class Search extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        HttpSession session = request.getSession(true);
        String keyword = request.getParameter("query");
        HashMap responseHashMap = Response.getResponseHashMap(keyword);
        if (!(responseHashMap.isEmpty())) {
            session.setAttribute("response", responseHashMap);
            session.setAttribute("keyword", keyword);
            response.sendRedirect("result.jsp");
        } else {
            response.sendRedirect("error.jsp");
        }
    }
}
