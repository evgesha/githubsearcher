package ParseHTML;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Logger;

public class GetCodePage {
    private String url = "https://docs.oracle.com/javase/8/docs/api/allclasses-frame.html";
    private String outFile = "src/recourse/js/classList.js";
    private static Logger log = Logger.getLogger(GetCodePage.class.getName());

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String searchNameClass(String line) {
        if (line.lastIndexOf("\">")!= -1 && line.lastIndexOf("</a>") != -1) {
            line = line.substring(line.lastIndexOf("\">") + 2, line.lastIndexOf("</a>"));
            if (line.lastIndexOf("</span>")!= -1 ) {
                line = line.substring(0, line.lastIndexOf("</span>"));
            }
            return line;
        }
        return null;
    }

    public void writerInFile(String line, int count,  FileWriter writer) throws IOException {
        if (count == 0) {
            writer.write("const allClass = [ " + "\n{\n\"label\": " + "\"" + line + "\"" +"," + "\n\"id\": " + (count+1) + "\n}");
        }
        else {
            writer.write(",\n{\n\"label\": " + "\"" + line + "\"" + "," + "\n\"id\": " + (count+1) + "\n}");
        }
    }

    public void execution() {
        BufferedReader reader = null;
        try {
            FileWriter writer = new FileWriter(outFile, false);
            URL site = new URL(url);
            reader = new BufferedReader(new InputStreamReader(site.openStream()));
            String line;
            for (int count = 0; (line = reader.readLine()) != null; count++) {
               line = searchNameClass(line);
                    if (line != null) {
                        writerInFile(line, count, writer);
                    }
                    else {
                        count--;
                    }
            }
            writer.write("\n]");
            writer.flush();
            reader.close();
        } catch (IOException ex) {
            log.info(ex.toString());
        }
    }

    public static void main(String[] args) throws IOException {
        new GetCodePage().execution();
    }
}