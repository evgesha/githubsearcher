package searchengine;

import org.elasticsearch.action.search.SearchType;


public class SearchParameters {

    private String index = "";

    private String type = "";

    private SearchType searchType = SearchType.DFS_QUERY_THEN_FETCH;

    private String rangeBy = "";

    private int rangeFrom = 0;

    private int rangeTo = 100;

    private int responseSize = 10;

    private boolean explain = true;

    public SearchParameters() {
    }

    public SearchParameters(String index, String type, SearchType searchType, String rangeBy, int rangeFrom,
                           int rangeTo, int responseSize, boolean explain) {
        this.index = index;
        this.type = type;
        this.searchType = searchType;
        this.rangeBy = rangeBy;
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.responseSize = responseSize;
        this.explain = explain;
    }

    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public SearchType getSearchType() {
        return searchType;
    }

    public void setSearchType(SearchType searchType) {
        this.searchType = searchType;
    }

    public String getRangeBy() {
        return rangeBy;
    }

    public void setRangeBy(String rangeBy) {
        this.rangeBy = rangeBy;
    }

    public int getRangeFrom() {
        return rangeFrom;
    }

    public void setRangeFrom(int rangeFrom) {
        this.rangeFrom = rangeFrom;
    }

    public int getRangeTo() {
        return rangeTo;
    }

    public void setRangeTo(int rangeTo) {
        this.rangeTo = rangeTo;
    }

    public int getResponseSize() {
        return responseSize;
    }

    public void setResponseSize(int responseSize) {
        this.responseSize = responseSize;
    }

    public boolean isExplain() {
        return explain;
    }

    public void setExplain(boolean explain) {
        this.explain = explain;
    }

    @Override
    public String toString() {
        return "SearchParameters{" +
                "index='" + index + '\'' +
                ", type='" + type + '\'' +
                ", searchType=" + searchType +
                ", rangeBy='" + rangeBy + '\'' +
                ", rangeFrom=" + rangeFrom +
                ", rangeTo=" + rangeTo +
                ", responseSize=" + responseSize +
                ", explain=" + explain +
                '}';
    }
}
