package searchengine;

import java.net.InetAddress;
import java.net.UnknownHostException;

import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

public class ElasticTRClient {
    public static final String POLY_HOST = "10.20.9.18";
    public static final String LOCAL_HOST = "localhost";

    public static TransportClient getTransportClient() throws UnknownHostException {
        TransportClient client = new PreBuiltTransportClient(Settings.EMPTY)
                .addTransportAddress(new TransportAddress(InetAddress.getByName(POLY_HOST), 9300));
        return client;
    }

    public void closeTransportClient(TransportClient client) {
        client.close();
    }

}
