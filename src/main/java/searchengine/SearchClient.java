package searchengine;

import java.net.UnknownHostException;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;

public class SearchClient {

	public static SearchResponse getSearchResponce(String keyword) throws UnknownHostException {
		Client client = ElasticTRClient.getTransportClient();
		SearchResponse searchResponse = client.prepareSearch().setQuery(QueryBuilders.queryStringQuery(keyword)).get();
		client.close();
		return searchResponse;
	}

}
