package utilits;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;

public class JSONUtil {

    public static JSONObject parseJSONFile(String filename) throws JSONException, IOException {
        String content = new String(Files.readAllBytes(Paths.get(filename)));
        return new JSONObject(content);
    }

    public static org.json.simple.JSONObject getJsonObjectFromFile(String path) {
        org.json.simple.JSONObject jsonObject = null;
        try {
            JSONParser parser = new JSONParser();
            Object obj = parser.parse(new FileReader(path));
            jsonObject = (org.json.simple.JSONObject) obj;
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    public static void writeJsonToFile(org.json.simple.JSONObject jsonObject, String path) {
        try (FileWriter file = new FileWriter(path)) {
            file.write(jsonObject.toJSONString() + "\n");
            file.flush();
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static org.json.simple.JSONObject getJsonObjFromUrl(String url) throws ParseException {
        Client client = ClientBuilder.newClient();
        String jsonString = client.target(url).request(MediaType.APPLICATION_JSON).get(String.class);
        return getJsonObjFromString(jsonString);
    }

    public static JSONArray getJsonArrayFromURL(String url) throws ParseException, InterruptedException {
        Client client = ClientBuilder.newClient();
        String url2;
        if (url.endsWith("/")) {
            url2 = url + "?client_id=1f352b366308bef9bfe0&client_secret=9bb2346ab416576ce2e8851f99553b5345c1505d";
        } else {
            url2 = url + "&client_id=1f352b366308bef9bfe0&client_secret=9bb2346ab416576ce2e8851f99553b5345c1505d";
        }
        String jsonString = client.target(url2).request(MediaType.APPLICATION_JSON).get(String.class);
        return getJsonArrayFromString(jsonString);
    }

    public static org.json.simple.JSONObject getJsonObjFromString(String string) throws ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(string);
        org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) obj;
        return jsonObject;
    }

    public static JSONArray getJsonArrayFromString(String string) throws ParseException {
        JSONParser parser = new JSONParser();
        Object obj = parser.parse(string);
        JSONArray jsonArray = (JSONArray) obj;
        return jsonArray;
    }

    public static HashMap getHashMapFromJSON(String t) throws JSONException {

        HashMap<String, String> map = new HashMap<String, String>();
        JSONObject jObject = new JSONObject(t);
        Iterator<?> keys = jObject.keys();

        while (keys.hasNext()) {
            String key = (String) keys.next();
            String value = jObject.getString(key);
            map.put(key, value);

        }
        return map;
    }

}

